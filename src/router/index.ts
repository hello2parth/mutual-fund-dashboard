import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import FundExplore from '../views/FundExplore.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/explore',
    name: 'FundExplore',
    component: FundExplore,
  },
  {
    path: '/explore/:code',
    name: 'FundDetail',
    component: () => import(/* webpackChunkName: "fund explore" */ '../views/FundDetail.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
