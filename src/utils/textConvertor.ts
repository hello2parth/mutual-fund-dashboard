export default {
  filters: {
    convertYandN(val: any) { // tslint:disable-line
      if (val === 'Y') {
        return 'Yes';
      } else if (val === 'N' ) {
        return 'No';
      } else {
        return val;
      }
    }
  },
}
