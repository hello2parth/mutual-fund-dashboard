import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    modal: { bShow: false, sTitle: '', sMsg: '' },
    showBusyLoader: false,
    dashboardData: [],
    isThemeLight: false,
  },
  mutations: {
    setModal(state, oConfig) {
      state.modal = {
        bShow: oConfig.show,
        sTitle: oConfig.title,
        sMsg: oConfig.message,
      };
    },
    setBusyLoader(state, show) {
      state.showBusyLoader = show;
    },
    setDashboardData(state, data) {
      state.dashboardData = data;
    },
    updateTheme(state, isThemeLight) {
      state.isThemeLight = isThemeLight;
    }
  },
  getters: {
    selectOptions(state) {
      const fundCategory = ['Select Fund Category'];
      const fundType = ['Select Fund type'];
      const plan = ['Select Plan'];
      /* tslint:disable:no-string-literal */
      if (!!state.dashboardData.length) {
        state.dashboardData.forEach((obj) => { // tslint:disable-line
          if (!!obj['fund_category'] && !fundCategory.some((v) => obj['fund_category'] === v)) {
            fundCategory.push(obj['fund_category']);
          }
          if (!!obj['fund_type'] && !fundType.some((v) => obj['fund_type'] === v)) {
            fundType.push(obj['fund_type']);
          }
          if (!!obj['plan'] && !plan.some((v) => obj['plan'] === v)) {
            plan.push(obj['plan']);
          }
        });
      /* tslint:enable:no-string-literal */
      }
      return {
        fundCategory,
        fundType,
        plan,
      }
    },
  },
  actions: {
  },
  modules: {
  },
});
