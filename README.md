# Mutual-fund-dashboard
Dashbord to display list of Mutual funds available in India. There are filters and sorting to get desired funds and fund detail page depending upon section.

## Features
* Built in Vue, with Vuex and Vue-router (All pages are accessed via stand alone URL)
* Mixin and Filter
* API integration and Data binding
* Async component loading
* Theming using SCSS
* Localization (i18n)
* CI/CD and hosting using Gitlab page
* Sorting on the basis of Fund Type, category and plan and dedicated search to filter out from all available data

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
