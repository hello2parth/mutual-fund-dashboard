import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Home from '@/views/Home.vue';

describe('Home.vue', () => {
  it('checking rendering correct html', () => {
    const wrapper = shallowMount(Home);
    expect(wrapper.html()).to.contain('<h1 class="card-title">Welcome to Mutual Fund Exploration</h1>')
  })
})
